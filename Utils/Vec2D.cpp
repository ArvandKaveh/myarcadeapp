/*
* Vec2D.cpp
*  Created by: Arvand Kaveh
*  Created on: 10/31/2020 4:14:35 PM
* Description:
*/

#pragma once
#include "Vec2D.h"
#include "Utils.h"

#include <cassert>
#include <cmath>

const Vec2D Vec2D::Zero;

bool Vec2D::operator==(const Vec2D& vec) const
{
	return IsEqual(vec.mX, mX) && IsEqual(vec.mY, mY);
}

bool Vec2D::operator!=(const Vec2D& vec) const
{
	return !(*this == vec);
}

Vec2D Vec2D::operator-() const
{
	return Vec2D(-mX, -mY);
}

Vec2D Vec2D::operator*(float scalar) const
{
	return Vec2D(scalar * mX, scalar * mY);
}

Vec2D Vec2D::operator/(float scalar) const
{
	assert( std::abs(scalar) > EPSILON && "Error, Division by zero");
	return Vec2D(mX / scalar, mY / scalar);
}

Vec2D& Vec2D::operator*=(float scalar)
{
	*this = *this * scalar;
	return *this;
}

Vec2D& Vec2D::operator/=(float scalar)
{
	assert(std::abs(scalar > EPSILON) && "Error, Division by zero");
	*this = *this / scalar;
	return *this;
}

Vec2D operator*(float scalar, const Vec2D& vec)
{
	return vec * scalar;
}

Vec2D Vec2D::operator+(const Vec2D& vec) const
{
	return Vec2D(mX + vec.mX, mY + vec.mY);
}

Vec2D Vec2D::operator-(const Vec2D& vec) const
{
	// Vec b - a = b + (-a)
	// return Vec2D(mX, mY) + (-vec);
	return Vec2D(mX - vec.mX, mY - vec.mY);
}

Vec2D& Vec2D::operator+=(const Vec2D& vec)
{
	*this = *this + vec;
	return *this;
}

Vec2D& Vec2D::operator-=(const Vec2D& vec)
{
	*this = *this - vec;
	return *this;
}

float Vec2D::Mag2() const
{
	// return mX * mX + mY * mY;
	return Dot(*this);
}

float Vec2D::Mag() const
{
	return sqrtf(Mag2());
}

Vec2D Vec2D::GetUnitVec() const
{
	float mag{ Mag() };

	if (mag > EPSILON)
	{
		return *this / mag;
	}

	return Vec2D::Zero;
}

Vec2D& Vec2D::Normalize()
{
	float mag{ Mag() };

	if (IsGreaterThanOrEqual(mag, EPSILON))
	{
		*this /= mag;
	}

	return *this;
}

float Vec2D::Distance(const Vec2D& vec) const
{
	// Distance: length (Magnitude) of b - a or a - b
	return (vec - *this).Mag();
}

float Vec2D::Dot(const Vec2D& vec) const
{
	return mX * vec.mX + mY * vec.mY;
}

Vec2D Vec2D::ProjectOnto(const Vec2D& vec2) const
{
	// Projection of v1 on v2 = (v1.i2)*i2

	Vec2D unitVec2{ vec2.GetUnitVec() };

	float dot{ Dot(unitVec2) };

	return unitVec2 * dot;
}

float Vec2D::AngleBetween(const Vec2D& vec2) const
{
	// a.b = ||a|| * ||b|| * cos(teta) 
	return acosf(GetUnitVec().Dot(vec2.GetUnitVec()));
}

Vec2D Vec2D::Reflect(const Vec2D& normal) const
{
	// v + -2 (v dot n)n
	// (v dot n)n = projection on n
	return *this - 2 * ProjectOnto(normal);
}

void Vec2D::Rotate(float angle, const Vec2D& aroundPoint)
{
	//         i^      j^
	// T = | cos(t) -sin(t)| 
	//     | sin(t)  cos(t)|
	// |xRot|   
	// |yRot| = Xi^ + Yj^
	
	// we Move our Vector to point around which we want to rotate
	*this -= aroundPoint;

	// We rotate our vector
	mX = mX * cosf(angle) - mY * sinf(angle);
	mY = mX * sinf(angle) + mY * sinf(angle);

	// we move back our vector back
	*this += aroundPoint;
}

Vec2D Vec2D::RotationResult(float angle, const Vec2D& aroundPoint) const
{
	//         i^      j^
	// T = | cos(t) -sin(t)| 
	//     | sin(t)  cos(t)|
	// |xRot|   
	// |yRot| = Xi^ + Yj^

	Vec2D tempVec(mX, mY);

	// we Move our Vector to point around which we want to rotate
	tempVec -= aroundPoint;

	// We rotate our vector
	float xRot{ tempVec.mX * cosf(angle) - tempVec.mY * sinf(angle) };
	float yRot{ tempVec.mX * sinf(angle) + tempVec.mY * sinf(angle) };

	// we move our vector back ot it's origin
	Vec2D rotated{ xRot, yRot };

	return rotated + aroundPoint;
}

std::ostream& operator<<(std::ostream out, const Vec2D v)
{
	out << "(" << v.mX << ", " << v.mY << ")";
	return out;
}