/*
* ScreenBuffer.cpp
*  Created by: Arvand Kaveh
*  Created on: 11/5/2020 9:48:38 PM
* Description:
*/

#pragma once
#include "ScreenBuffer.h"
#include <SDL.h>
#include <cassert>

ScreenBuffer::ScreenBuffer(): mSurface(nullptr)
{

}

ScreenBuffer::ScreenBuffer(const ScreenBuffer& screenBuffer)
{
	// Creates a surface with the passed in screenbuffer
	mSurface = SDL_CreateRGBSurfaceWithFormat(0, screenBuffer.mSurface->w, screenBuffer.mSurface->h, 0, screenBuffer.mSurface->format->format);

	// Populates all the pixles the newly created buffer with the passed in pixles
	// nullptr makes the function to copy the entire input to the output
	SDL_BlitSurface(screenBuffer.mSurface, nullptr, mSurface, nullptr);
}

ScreenBuffer::~ScreenBuffer()
{
	if (mSurface)
	{
		SDL_FreeSurface(mSurface);
	}
}

ScreenBuffer& ScreenBuffer::operator==(const ScreenBuffer& screenBuffer)
{
	if (this == &screenBuffer)
	{
		return *this;
	}

	if (mSurface)
	{
		SDL_FreeSurface(mSurface);
		mSurface = nullptr;
	}

	if (screenBuffer.mSurface != nullptr)
	{

		mSurface = SDL_CreateRGBSurfaceWithFormat(0, screenBuffer.mSurface->w, screenBuffer.mSurface->h, 0, screenBuffer.mSurface->format->format);

		SDL_BlitSurface(screenBuffer.mSurface, nullptr, mSurface, nullptr);
	}

	return *this;
}

void ScreenBuffer::Init(uint32_t format, uint32_t width, uint32_t height)
{
	mSurface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 0, format);
	Clear();	// set the screen with initial color
}

void ScreenBuffer::Clear(const Color& c)
{
	assert(mSurface);
	if (mSurface)
	{
		SDL_FillRect(mSurface, nullptr, c.GetPixelColor());
	}
}

void ScreenBuffer::SetPixel(const Color& c, int x, int y)
{
	assert(mSurface);

	// Bound the x and y
	if (mSurface && (y < mSurface->h) && (x < mSurface->h) && y >= 0 &&  x >= 0)
	{
		// We need to lock the access to the surface while performing the following
		SDL_LockSurface(mSurface);

		// We get the all the pixels directly and cast it to uint32_t pointer immediately
		// This way we can modify the pixel directly
		uint32_t * pixels = (uint32_t*)mSurface->pixels;

		size_t index{ GetIndex(x, y) };

		pixels[index] = c.GetPixelColor();

		SDL_UnlockSurface(mSurface);
	}
}

// The Surface is not a 2D array instead is a big 1D array with 
// all the rows appendend to the first row
// to get the relvant Index for Row r and Column c: 
// Index(r,c) = (r * widthOfArray) + c
uint32_t ScreenBuffer::GetIndex(int c, int r)
{
	assert(mSurface);
	if (mSurface)
	{
		return r * (mSurface->w) + c;
	}
}