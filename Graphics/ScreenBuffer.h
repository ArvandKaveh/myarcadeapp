/*
* ScreenBuffer.h
*  Created by: Arvand Kaveh
*  Created on: 11/5/2020 9:31:15 PM
* Description: Screenbuffer class handles the data itself, so we need to have 
* the overloaded == operator and copy cunstructor and deconstructor
*/

#pragma once
#include <stdint.h>
#include <Color.h>

// Forward declarations

// SDL_Surface is in short a 2D Array of pixels
// This is effectively the Canvas on which we draw
class SDL_Surface;	

class ScreenBuffer
{
public:
	ScreenBuffer();
	ScreenBuffer(const ScreenBuffer& screenBuffer);
	~ScreenBuffer();

	ScreenBuffer& operator==(const ScreenBuffer& screenBuffer);

	void Init(uint32_t format, uint32_t width, uint32_t height);

	inline SDL_Surface * GetSurface() { return mSurface; }

	void Clear(const Color& c = Color::Black());

	void SetPixel(const Color& c, int x, int y);

private:
	SDL_Surface * mSurface;

	// The Surface is not a 2D array instead is a big 1D array with 
	// all the rows appendend to the first row
	// to get the relvant Index for Row r and Column c: 
	// Index(r,c) = (r * widthOfArray) + c
	uint32_t GetIndex(int c, int r);
};