/*
* Screen.h
*  Created by: Arvand Kaveh
*  Created on: 11/24/2020 8:55:13 PM
* Description: The Screen handles the SDL_Window
*/

#pragma once

#include <stdint.h>
#include <ScreenBuffer.h>	// We inlcude this to allow "Double buffering"
#include "Color.h"

class Vec2D;
struct SDL_Window;
struct SDL_Surface;

class Screen
{

public:
	Screen();
	~Screen();

	// To prevent copying
	// other method is to forward declare the copy constructure in private (c++11 way)
	Screen(const Screen& screen) = delete;
	Screen& operator==(const Screen& screen) = delete;

	SDL_Window* Init(uint32_t w, uint32_t h, uint32_t mag);
	void SwapScreens();

	inline void SetClearColor(const Color& clearColor) { mClearColor = clearColor; }
	inline uint32_t GetWidth() const { return mWidth; }
	inline uint32_t GetHeight() const { return mHeight; }

	// Draw methods
	void Draw(int x, int y, const Color& color);
	void Draw(const Vec2D& point, const Color& color);


private:

	uint32_t mWidth;
	uint32_t mHeight;

	Color mClearColor;
	ScreenBuffer mBackBuffer;

	SDL_Window* moptrWindow;
	SDL_Surface* mnoptrWindowSurface;

	void ClearScreen();

};
