/*
* Line2D.cpp
*  Created by: Arvand Kaveh
*  Created on: 11/10/2020 10:22:05 PM
* Description:
*/

#pragma once
#include "Line2D.h"
#include <math.h>
#include <cmath>
#include "Utils.h"

Line2D::Line2D(Vec2D & p0, Vec2D & p1):mP0{p0}, mP1{p1}
{

}

Line2D::Line2D(float x0, float y0, float x1, float y1):mP0{Vec2D(x0, y0)}, mP1{Vec2D(x1, y1)}
{

}

bool Line2D::operator==(Line2D & line) const
{
	return mP0 == line.GetP0() && mP1 == line.GetP1();
}

bool Line2D::operator!=(Line2D & line) const
{
	return !(*this == line);
}

float Line2D::MinDistanceFrom(const Vec2D & p, bool limitToSegment) const
{
	return p.Distance(ClosestPoint(p, limitToSegment));
}

Vec2D Line2D::ClosestPoint(const Vec2D & p, bool limitToSegment) const
{
	//		t      |
	//---------------------------------
	// a.b / ||b|| = ||a|| * cos(teta)
	// closestPoint = unitVector(b) * t

	Vec2D P0ToP{ p - mP0 };
	Vec2D P0ToP01{ mP0 - mP1 };

	float lengthB{ P0ToP01.Mag2() };
	float dot{ P0ToP.Dot(P0ToP01) };

	float t{ dot / lengthB };

	if (limitToSegment)
	{
		t = std::fmax(0, std::min(1.0f, t));
	}

	return mP0 + P0ToP01 * t;
}

Vec2D Line2D::Midpoint() const
{
	// xMid = (x1 + x2) / 2
	// yMid = (y1 + y2) / 2

	return Vec2D((mP0.GetX() + mP1.GetX()) / 2.0f, (mP0.GetY() + mP1.GetY()) / 2.0f);
}

float Line2D::Slope() const
{
	// rise / run
	float dx{ mP0.GetX() - mP1.GetX() };
	
	if (std::abs(dx) < EPSILON)
	{
		return 0;
	}

	float dy{ mP0.GetY() - mP1.GetY() };

	return dy / dx;
}

float Line2D::Length() const
{
	return mP0.Distance(mP1);
}